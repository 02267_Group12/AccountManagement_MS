package messaging.utils;
import io.cucumber.messages.internal.com.google.gson.Gson;

/**
 * Simple static functions for Gson serialization
 * @author: Peter
 */
public class JsonUtils {
    private static final Gson gson = new Gson();

    public static String toJson(Object obj, Class<?> class_type){
        return gson.toJson(obj, class_type);
    }

    public static Object fromJson(String json, Class<?> class_type){
        return gson.fromJson(json, class_type);
    }
}

