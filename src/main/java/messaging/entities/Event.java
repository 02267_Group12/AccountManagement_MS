package messaging.entities;

/**
 * Custom events
 * @author: Peter
 */
public class Event {

    private String owner = "";
    private EVENT_TYPE eventType;
    private String content;
    private String content_class;
    private String extra;

    public Event(EVENT_TYPE eventType, String owner, String content, String content_class) {
        this.eventType = eventType;
        this.owner = owner;
        this.content = content;
        this.content_class = content_class;
        this.extra = null;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public EVENT_TYPE getEventType() {
        return eventType;
    }

    public void setEventType(EVENT_TYPE eventType) {
        this.eventType = eventType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent_class() {
        return content_class;
    }

    public void setContent_class(String content_class) {
        this.content_class = content_class;
    }

    public boolean equals(Object o) {
        if (!this.getClass().equals(o.getClass())) {
            return false;
        }
        Event other = (Event) o;
        return this.eventType.equals(other.eventType)
                && this.owner.equals(other.owner)
                && this.content.equals(other.content)
                && this.content_class.equals(other.content_class);
    }

    public int hashCode() {
        return content.hashCode();
    }

    public String toString() {
        return this.getClass().getName() + " ("+this.owner+") "
                +this.eventType+" = [["+ this.content+"]] -> "+this.content_class;
    }
}
