package messaging.entities;

public interface EventReceiver {
    void receiveEvent(Event event) throws Exception;
    Event receiveAndReplyBack(Event event);
}