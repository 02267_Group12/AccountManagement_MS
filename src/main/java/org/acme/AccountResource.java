package org.acme;

import dtu.rs.entities.DTUPayAccount;
import dtu.ws.fastmoney.User;
import manager.AccountsManager;
import messaging.entities.EVENT_TYPE;
import messaging.entities.Event;
import messaging.mq.MQSender;

import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Account Management REST API
 * @author: Peter
 */
@Path("/accounts")
public class AccountResource {
    private AccountsManager accountsManager = new AccountsManager();
    private MQSender mqSender = new MQSender();

    private void deleteCustomerTokens(String cid){
        Event event = new Event(EVENT_TYPE.DELETE_TOKENS, "Account Manager", cid, String.class.getName());
        mqSender.sendEvent(event, "token.delete");
    }

    @Path("/customer")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response registerCustomer(User user) {
        try {
            var cid = accountsManager.addNewCustomer(user);
            return Response
                    .ok(new GenericEntity<String>(cid) {}).build();
        } catch (Exception e){
            return Response
                    .status(403, e.getMessage())
                    .build();
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/customer/{id}")
    public Response getCustomerAccount(@PathParam("id") String id){
        DTUPayAccount account = accountsManager.getAccount(id);
        if(account == null){
            return Response.status(404, "The user with id can not be found: "+id).build();
        }
        return Response.ok(new GenericEntity<DTUPayAccount>(accountsManager.getAccount(id)) {}).build();
    }

    @Path("/merchant")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response registerMerchant(User user) {
        try {
            var mid = accountsManager.addNewMerchant(user);
            return Response
                    .ok(new GenericEntity<String>(mid) {}).build();
        } catch (Exception e){
            return Response
                    .status(403, e.getMessage())
                    .build();
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/merchant/{id}")
    public Response getMerchantAccount(@PathParam("id") String id){
        DTUPayAccount account = accountsManager.getAccount(id);
        if(account == null){
            return Response.status(404, "The user with id can not be found: "+id).build();
        }
        return Response.ok(new GenericEntity<DTUPayAccount>(accountsManager.getAccount(id)) {}).build();
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response changeAccountInformation(@PathParam("id") String id, User user){
        accountsManager.modifyAccount(id, user);
        return Response.ok().build();
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response retireAccount(@PathParam("id") String id){
        var isCustomer = accountsManager.isCustomerAccount(id);
        if(accountsManager.popAccount(id) != null){
            if(isCustomer)
                deleteCustomerTokens(id);
            return Response.accepted().build();
        }else{
            return Response.serverError().build();
        }
    }

    @GET
    public String getResponse(){
        return "Success";
    }
}