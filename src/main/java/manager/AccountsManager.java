package manager;

import dtu.rs.entities.CustomerAccount;
import dtu.rs.entities.DTUPayAccount;
import dtu.rs.entities.DTUPayAccountInfo;
import dtu.rs.entities.MerchantAccount;
import dtu.ws.fastmoney.User;
import messaging.entities.EVENT_TYPE;
import messaging.entities.Event;
import messaging.entities.EventProcessor;
import messaging.mq.MQSender;
import messaging.utils.JsonUtils;
import org.jboss.resteasy.plugins.providers.sse.SseConstants;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Account Manager
 * @author: Peter
 */
public class AccountsManager implements EventProcessor {
    private static final String SERVICE_NAME = "Account Manager";
    private static List<DTUPayAccount> accounts = new ArrayList<>();

    private String generateID(){
        return UUID.randomUUID().toString();
    }

    public String addNewCustomer(User user) throws Exception {
        var actualAccount = getAccountFromCPR(user.getCprNumber());

        if(actualAccount != null)
            throw new Exception(user.getCprNumber() + " CPR number already exists.");

        CustomerAccount newAccount = new CustomerAccount();
        newAccount.setUser(user);
        newAccount.setId(generateID());
        accounts.add(newAccount);
        return newAccount.getId();
    }

    public String addNewMerchant(User user) throws Exception {
        var actualAccount = getAccountFromCPR(user.getCprNumber());

        if(actualAccount != null)
            throw new Exception(user.getCprNumber() + " CPR number already exists.");

        MerchantAccount newAccount = new MerchantAccount();
        newAccount.setUser(user);
        newAccount.setId(generateID());
        accounts.add(newAccount);
        return newAccount.getId();
    }

    public DTUPayAccount getAccountFromCPR(String cpr){
        for(int i=0; i<accounts.size(); i++){
            if(accounts.get(i).getUser().getCprNumber().equals(cpr))
                return  accounts.get(i);
        }
        return null;
    }

    public DTUPayAccount getAccount(String id){
        for(int i=0; i<accounts.size(); i++){
            if(accounts.get(i).getId().equals(id))
                return accounts.get(i);
        }
        return null;
    }

    public void modifyAccount(String id, User userInformation){
        getAccount(id).setUser(userInformation);
    }

    public DTUPayAccount popAccount(String id){
        for(int i=0; i<accounts.size(); i++){
            if(accounts.get(i).getId().equals(id))
                return accounts.remove(i);
        }
        return null;
    }

    public boolean isCustomerAccount(String cid){
        return getAccount(cid) instanceof CustomerAccount;
    }

    public boolean isMerchantAccount(String cid){
        return getAccount(cid) instanceof MerchantAccount;
    }

    public List<DTUPayAccount> getAllAccounts(){
        return accounts;
    }

    @Override
    public void processMessage(String owner, EVENT_TYPE type, Object content, String extra) throws Exception {
        if(type == EVENT_TYPE.GET_ACCOUNT){
            var user_id = (String) content;
            var account = getAccount(user_id);
            var account_info = new DTUPayAccountInfo(account.getId(), account.getUser().getCprNumber(), account.getUser().getFirstName(), account.getUser().getLastName());
            String account_info_str = JsonUtils.toJson(account_info, DTUPayAccountInfo.class);

            Event event;

            if(isCustomerAccount(user_id))
                event = new Event(EVENT_TYPE.RESPONSE_GET_ACCOUNT_CUSTOMER, "Account Manager", account_info_str, DTUPayAccountInfo.class.getName());
            else
                event = new Event(EVENT_TYPE.RESPONSE_GET_ACCOUNT_MERCHANT, "Account Manager", account_info_str, DTUPayAccountInfo.class.getName());
            event.setExtra(extra);
            new MQSender().sendEvent(event, "response.get_account");
        }
    }
}
