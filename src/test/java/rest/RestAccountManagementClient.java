package rest;

import dtu.rs.entities.DTUPayAccount;
import dtu.ws.fastmoney.User;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class RestAccountManagementClient {

    WebTarget baseUrl;


    private static String getRootPath(){
        String env = System.getenv("RUNNING_ENV");
        if (env == null) return "http://localhost:";
        else return "http://accountmanagement:";
    }

    public RestAccountManagementClient() {
        Client client = ClientBuilder.newClient();
        baseUrl = client.target(getRootPath()+"5002/");
    }

    public String registerNewCustomer(User user){
        String response = baseUrl.path("accounts/customer").request()
                .post(Entity.entity(user, MediaType.APPLICATION_JSON), String.class);

        return response;
    }

    public DTUPayAccount retrieveCustomerAccount(String id){
        Response response = baseUrl.path("accounts/customer/"+id).request()
                .accept(MediaType.APPLICATION_JSON)
                .get(Response.class);

        if(response.getStatus() == 200) {
            DTUPayAccount account = response.readEntity(new GenericType<DTUPayAccount>() {
            });

            return account;
        }else{
            return null;
        }
    }

    public String registerNewMerchant(User user){
        String response = baseUrl.path("accounts/merchant").request()
                .post(Entity.entity(user, MediaType.APPLICATION_JSON), String.class);

        return response;
    }

    public DTUPayAccount retrieveMerchantAccount(String id){
        Response response = baseUrl.path("accounts/merchant/"+id).request()
                .accept(MediaType.APPLICATION_JSON)
                .get(Response.class);

        if(response.getStatus() == 200) {
            DTUPayAccount account = response.readEntity(new GenericType<DTUPayAccount>() {
            });

            return account;
        }else{
            return null;
        }
    }

    public Response changeAccountInformation(String id, User userInformation){
        Response response = baseUrl.path("accounts/"+id).request()
                .accept(MediaType.APPLICATION_JSON)
                .put(Entity.entity(userInformation, MediaType.APPLICATION_JSON), Response.class);

        return response;
    }

    public Response retireAccount(String id){
        Response response = baseUrl.path("accounts/"+id).request()
                .accept(MediaType.APPLICATION_JSON)
                .delete(Response.class);

        return response;
    }
}

