package account;

import dtu.rs.entities.DTUPayAccount;
import dtu.ws.fastmoney.User;
import io.cucumber.java.Before;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;
import rest.RestAccountManagementClient;

public class AccountSteps {

    User actualCustomer;
    User actualMerchant;

    static String actualCustomerCID;
    static String actualMerchantMID;
    DTUPayAccount actualCustomerAccount;
    DTUPayAccount actualMerchantAccount;

    static RestAccountManagementClient accountManagementClient = new RestAccountManagementClient();

    @When("the customer {string} {string} with CPR {string} registers themself to DTUPay")
    public void theCustomerWithCPRRegistersThemselfToDTUPay(String firstName, String lastName, String cpr) {
        actualCustomer = new User();
        actualCustomer.setFirstName(firstName);
        actualCustomer.setLastName(lastName);
        actualCustomer.setCprNumber(cpr);

        try {
            actualCustomerCID = accountManagementClient.registerNewCustomer(actualCustomer);
            System.out.println("CID: " + actualCustomerCID);
        }catch (Exception error){
            System.out.println(error);
        }
    }

    @When("the merchant {string} {string} with CPR {string} registers themself to DTUPay")
    public void theMerchantWithCPRNumberRegistersThemselfToDTUPay(String firstName, String lastName, String cpr) {
        actualMerchant = new User();
        actualMerchant.setFirstName(firstName);
        actualMerchant.setLastName(lastName);
        actualMerchant.setCprNumber(cpr);

        try {
            actualMerchantMID = accountManagementClient.registerNewMerchant(actualMerchant);
            System.out.println("MID: " + actualMerchantMID);
        }catch (Exception error){
            System.out.println(error);
        }
    }

    @Then("the registration is successful and the customer can see their cid")
    public void theRegistrationIsSuccessful() {
        Assertions.assertNotNull(actualCustomerCID);
    }

    @Then("the registration is successful and the merchant can see their mid")
    public void theRegistrationIsSuccessfulAndTheMerchantCanSeeTheirCid() {
        Assertions.assertNotNull(actualMerchantMID);
    }

    @Then("the customer registration is not successful")
    public void theCustomerRegistrationIsNotSuccessful() {
        //Assertions.assertNull(actualCustomerCID);
    }

    @Then("the merchant registration is not successful")
    public void theMerchantRegistrationIsNotSuccessful() {
        //Assertions.assertNull(actualMerchantMID);
    }

    @When("the customer asks for their DTUPay account")
    public void theCustomerAsksForTheirDTUPayAccount() {
        actualCustomerAccount = accountManagementClient.retrieveCustomerAccount(actualCustomerCID);
    }

    @When("the merchant asks for their DTUPay account")
    public void theMerchantAsksForTheirDTUPayAccount() {
        actualMerchantAccount = accountManagementClient.retrieveMerchantAccount(actualMerchantMID);
    }

    @Then("the customer can retrieve their DTUPay account")
    public void theCustomerCanRetrieveTheirDTUPayAccount() {
        Assertions.assertEquals(actualCustomerCID, actualCustomerAccount.getId());
    }

    @Then("the merchant can retrieve their DTUPay account")
    public void theMerchantCanRetrieveTheirDTUPayAccount() {
        Assertions.assertEquals(actualMerchantMID, actualMerchantAccount.getId());
    }

    @When("the manager deletes the customer account")
    public void theManagerDeletesTheAccount() {
        accountManagementClient.retireAccount(actualCustomerCID);
    }

    @Then("the account is removed from storage")
    public void theAccountIsRemovedFromStorage() {
        Assertions.assertNull(accountManagementClient.retrieveCustomerAccount(actualCustomerCID));
    }

    @When("the manager modifies the customer account to {string} {string} with CPR {string}")
    public void theManagerModifiesTheAccountWithWithCPR(String firstName, String lastName, String cpr) {
        User modifiedCustomer = new User();
        modifiedCustomer.setFirstName(firstName);
        modifiedCustomer.setLastName(lastName);
        modifiedCustomer.setCprNumber(cpr);

        accountManagementClient.changeAccountInformation(actualCustomerCID, modifiedCustomer);
    }

    @Then("the account has been modified")
    public void theAccountHasBeenModified() {
        Assertions.assertNotEquals(actualCustomer, accountManagementClient.retrieveCustomerAccount(actualCustomerCID).getUser());
    }

    @When("the manager deletes the customer {string} account")
    public void theManagerDeletesTheCustomerAccount(String cid) {
        accountManagementClient.retireAccount(cid);
    }

    @Then("the account {string} does not exist")
    public void theAccountDoesNotExist(String cid) {
        Assertions.assertNull(accountManagementClient.retrieveCustomerAccount(cid));
    }

    //@Before
    //public void takeASleepBeforeTest() {
    //    try {
    //        Thread.sleep(1000);
    //    } catch (InterruptedException e) {
    //        e.printStackTrace();
    //    }
    //}

    //@After
    //public void removeAccounts() {
    //    String CPR_1 = "061111-0089";
    //    String CPR_2 = "730521-0041";
    //    try {
    //       String id_1 = dtuPay.getAccountFromCpr(CPR_1).getId();
    //        String id_2 = dtuPay.getAccountFromCpr(CPR_2).getId();
    //        dtuPay.removeBankAccount(id_1);
    //        dtuPay.removeBankAccount(id_2);
    //   } catch (Exception error) {
    //        System.out.println("Already deleted.");
    //    }
    //}
}
