Feature: Account management
  Scenario: REST The customer wants to register
    When the customer "Andrew" "Ryan" with CPR "061111-0089" registers themself to DTUPay
    Then the registration is successful and the customer can see their cid

  Scenario: REST The customer wants to get their account
    When the customer asks for their DTUPay account
    Then the customer can retrieve their DTUPay account

  Scenario: REST The customer can not register without bank account
    When the customer "Jackie" "Welles" with CPR "068811-1100" registers themself to DTUPay
    Then the customer registration is not successful

  Scenario: REST The merchant wants to register
    When the merchant "Frank" "Fontaine" with CPR "730521-0041" registers themself to DTUPay
    Then the registration is successful and the merchant can see their mid

  Scenario: REST The merchant wants to get their account
    When the merchant asks for their DTUPay account
    Then the merchant can retrieve their DTUPay account

  Scenario: REST The merchant can not register without bank account
    When the merchant "Jackie" "Welles" with CPR "068811-1100" registers themself to DTUPay
    Then the merchant registration is not successful

  Scenario: REST The manager wants to update the user information
    When the customer "Andrew" "Ryan" with CPR "061111-0089" registers themself to DTUPay
    When the manager modifies the customer account to "Violet" "Evergarden" with CPR "061111-0089"
    Then the account has been modified

  Scenario: REST The manager wants to delete an account
    When the manager deletes the customer account
    Then the account is removed from storage

  Scenario: REST The manager wants to delete a not existing account
    When the manager deletes the customer "000" account
    Then the account "000" does not exist