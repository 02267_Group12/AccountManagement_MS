#!/bin/bash
set -e

mvn package
docker-compose up -d --build
sleep 3
mvn test -DskipTests=false
docker-compose down --rmi all

